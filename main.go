package main

import (
	"encoding/xml"
	"github.com/gin-gonic/gin"
)


func main() {
	router := gin.Default()
	router.GET("/", IndexHandler)
	router.Run()
}
type Person struct {
	XMLName xml.Name `xml:"person"`
	FirstName	string	`xml:"first_name,attr"`
	LastName	string	`xml:"last_name,attr"`
}

func IndexHandler(c *gin.Context) {
	c.XML(200, Person{FirstName: "Farich",
							LastName: "Rinaldy"})
}