package main

import (
	"github.com/gin-gonic/gin"
	"time"
)

func main(){
	router := gin.Default()
	router.Run(":5001")
}

type Recipe struct {
	Name			string		`json:"name"`
	Tags			[]string	`json:"tags"`
	Ingredients		[]string	`json:"ingredients"`
	Instructions	[]string	`json:"instructions"`
	PublisedAt		time.Time	`json:"publised_at"`
}